<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%users}}`.
 */
class m230703_120230_create_users_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%user}}', [
            'id' => $this->primaryKey(),
            'nombre' => $this->string(255),
            'apellidos' => $this->string(255),
            'email' => $this->string(100),
            'username' => $this->string(100),
            'authKey' => $this->string(255),
            'activo' => $this->boolean()->defaultValue(true),
            'password' => $this->string(255),
            'accessToken' => $this->string(255),
            'administrador' => $this->boolean()->defaultValue(false)
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%user}}');
    }
}


