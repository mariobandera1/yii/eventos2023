<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Tienelugar $model */

$this->title = 'Create Tienelugar';
$this->params['breadcrumbs'][] = ['label' => 'Tienelugars', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tienelugar-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
