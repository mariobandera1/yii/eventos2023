<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Tienelugar $model */

$this->title = 'Update Tienelugar: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Tienelugars', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="tienelugar-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
