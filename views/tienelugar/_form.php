<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\Tienelugar $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="tienelugar-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'id')->textInput() ?>

    <?= $form->field($model, 'idEvento')->textInput() ?>

    <?= $form->field($model, 'idSala')->textInput() ?>

    <?= $form->field($model, 'montajeSala')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
