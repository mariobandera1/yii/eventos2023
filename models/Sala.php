<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "sala".
 *
 * @property int $id
 * @property string|null $nombre
 * @property string|null $descripcion
 *
 * @property Evento[] $idEventos
 * @property Tienelugar[] $tienelugars
 */
class Sala extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'sala';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'required'],
            [['id'], 'integer'],
            [['descripcion'], 'string'],
            [['nombre'], 'string', 'max' => 400],
            [['id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'descripcion' => 'Descripcion',
        ];
    }

    /**
     * Gets query for [[IdEventos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdEventos()
    {
        return $this->hasMany(Evento::class, ['id' => 'idEvento'])->viaTable('tienelugar', ['idSala' => 'id']);
    }

    /**
     * Gets query for [[Tienelugars]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTienelugars()
    {
        return $this->hasMany(Tienelugar::class, ['idSala' => 'id']);
    }
}
