<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tienelugar".
 *
 * @property int $id
 * @property int|null $idEvento
 * @property int|null $idSala
 * @property string|null $montajeSala
 *
 * @property Evento $idEvento0
 * @property Sala $idSala0
 */
class Tienelugar extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tienelugar';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'required'],
            [['id', 'idEvento', 'idSala'], 'integer'],
            [['montajeSala'], 'string', 'max' => 400],
            [['idEvento', 'idSala'], 'unique', 'targetAttribute' => ['idEvento', 'idSala']],
            [['id'], 'unique'],
            [['idEvento'], 'exist', 'skipOnError' => true, 'targetClass' => Evento::class, 'targetAttribute' => ['idEvento' => 'id']],
            [['idSala'], 'exist', 'skipOnError' => true, 'targetClass' => Sala::class, 'targetAttribute' => ['idSala' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'idEvento' => 'Id Evento',
            'idSala' => 'Id Sala',
            'montajeSala' => 'Montaje Sala',
        ];
    }

    /**
     * Gets query for [[IdEvento0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdEvento0()
    {
        return $this->hasOne(Evento::class, ['id' => 'idEvento']);
    }

    /**
     * Gets query for [[IdSala0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdSala0()
    {
        return $this->hasOne(Sala::class, ['id' => 'idSala']);
    }
}
