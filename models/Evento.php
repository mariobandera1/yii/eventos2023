<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "evento".
 *
 * @property int $id
 * @property string $codigoReserva
 * @property string|null $nombre
 * @property string|null $descripcion
 * @property string|null $fechaHora
 * @property string|null $fechaRelease
 *
 * @property Sala[] $idSalas
 * @property Tienelugar[] $tienelugars
 */
class Evento extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'evento';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'codigoReserva'], 'required'],
            [['id'], 'integer'],
            [['descripcion'], 'string'],
            [['fechaHora', 'fechaRelease'], 'safe'],
            [['codigoReserva'], 'string', 'max' => 100],
            [['nombre'], 'string', 'max' => 300],
            [['codigoReserva'], 'unique'],
            [['id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'codigoReserva' => 'Codigo Reserva',
            'nombre' => 'Nombre',
            'descripcion' => 'Descripcion',
            'fechaHora' => 'Fecha Hora',
            'fechaRelease' => 'Fecha Release',
        ];
    }

    /**
     * Gets query for [[IdSalas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdSalas()
    {
        return $this->hasMany(Sala::class, ['id' => 'idSala'])->viaTable('tienelugar', ['idEvento' => 'id']);
    }

    /**
     * Gets query for [[Tienelugars]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTienelugars()
    {
        return $this->hasMany(Tienelugar::class, ['idEvento' => 'id']);
    }
}
