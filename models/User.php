<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

class User extends ActiveRecord  implements \yii\web\IdentityInterface
{
    public $password_repeat; // propiedad para que cuando creo el usuario repita la contraseña
    public $codigo; // es para el captcha

    public static function tableName()
    {
        return 'user';
    }


    public function scenarios()
    {
        //colocar los campos que necesito que pase en la asignacion masiva
        return [
            'crear' => ['password_repeat', 'password', 'nombre', 'email', 'username', 'codigo'],
            'actualizar' => ['password', 'nombre', 'email', 'username', 'apellidos'],
        ];
    }

    public function rules()
    {
        return [
            [['nombre', 'password', 'apellidos'], 'string', 'max' => 255, 'on' => 'crear'],
            [['nombre', 'password', 'password_repeat', 'email', 'username'], 'required', 'message' => 'El campo {attribute} es obligatorio', 'on' => 'crear'],
            // que el usuario no exista
            [['username', 'email'], 'unique', 'message' => 'El {attribute} ya existe en el sistema', 'on' => 'crear'],
            ['password', 'string', 'min' => 4, 'message' => 'la contraseña debe tener al menos 6 caracteres', 'on' => 'crear'],
            ['email', 'email', 'message' => 'Escribe un correo correctamente', 'on' => 'crear'],
            //comparacion de contraseñas
            ['password_repeat', 'compare', 'compareAttribute' => 'password', 'operator' => '==', 'message' => 'Las contraseñas deben coincidir', 'on' => 'crear'],
            // validacion de captcha
            //['codigo', 'captcha', 'message' => 'No coincide el codigo mostrado'], // esto funcionaria correctamente sin AJAX
            //Con AJAX existe un bug lo corregimos con una funcion 
            //['codigo', 'codeVerify', 'on' => 'crear'],
            //['codigo', 'required', 'message' => 'Debes escribir algo en los codigos', 'on' => 'crear'],

            //[['password_repeat', 'password', 'nombre', 'email', 'username', 'codigo'], 'safe'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'nombre' => 'Nombre',
            'password' => 'Contraseña',
            'password_repeat' => 'Repite la contraseña',
            'codigo' => 'Escribe los codigos que ves',
        ];
    }

    /**
     * Regla de validacion para el captcha
     * @param mixed $attribute
     * @return void
     */
    public function codeVerify($attribute)
    {
        /* nombre de la accion del controlador */
        $captcha_validate = new \yii\captcha\CaptchaAction('captcha', Yii::$app->controller);

        if ($this->$attribute) {
            $code = $captcha_validate->getVerifyCode();

            if ($this->$attribute != $code) {
                $this->addError($attribute, $code . 'Ese codigo de verificacion no es correcto');
            }
        }
    }

    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id]);
    }

    public static function findByUsername($username)
    {
        return static::findOne(['username' => $username]);
    }
    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /* public function validatePassword($password)
      {
      return Yii::$app->security->validatePassword($password, $this->password_hash);
      } */

    public function validatePassword($password)
    {
        return Yii::$app->getSecurity()->validatePassword($password, $this->password);
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->authKey;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    public static function findIdentityByAccessToken($token, $type = null)
    {
        return null;
    }

    public function beforeSave($insert)
    {

        if (parent::beforeSave($insert)) {

            if ($this->isNewRecord) {

                $this->authKey = Yii::$app->security->generateRandomString();
                $this->password = Yii::$app->security->generatePasswordHash($this->password);
            } else {
                if (!empty($this->getDirtyAttributes(["password"]))) {
                    $this->password = Yii::$app->security->generatePasswordHash($this->password);
                }
            }

            return true;
        }

        return false;
    }

    /**
     * Comprobamos si el usuario es administrador
     * @param mixed $id
     * @return bool
     */
    public static function isUserAdmin($id)
    {
        if (User::findOne(['id' => $id, 'activo' => true, 'administrador' => true])) {
            return true;
        } else {

            return false;
        }
    }

    /**
     * Summary of isUserSimple
     * @param mixed $id
     * @return bool
     */
    public static function isUserSimple($id)
    {
        if (User::findOne(['id' => $id, 'activo' => true, 'administrador' => false])) {
            return true;
        } else {

            return false;
        }
    }
}
